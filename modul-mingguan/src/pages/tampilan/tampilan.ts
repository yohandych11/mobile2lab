import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the TampilanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tampilan',
  templateUrl: 'tampilan.html',
})
export class TampilanPage {
  nama: string;
  nim: string;
  classof: string;
  judul: string;
  pembimbing1: string;
  pembimbing2: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    this.nama = this.navParams.get('data')['namaDiri'];
    this.nim = this.navParams.get('data')['NIMDiri'];
    this.judul = this.navParams.get('data')['judulSkripsi'];
    this.classof = this.navParams.get('data')['classOf'];
    this.pembimbing1 = this.navParams.get('data')['pembimbing'][0];
    this.pembimbing2 = this.navParams.get('data')['pembimbing'][1] == null ? '' : this.navParams.get('data')['pembimbing'][1];
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

}
