import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TampilanPage } from './tampilan';

@NgModule({
  declarations: [
    TampilanPage,
  ],
  imports: [
    IonicPageModule.forChild(TampilanPage),
  ],
})
export class TampilanPageModule {}
