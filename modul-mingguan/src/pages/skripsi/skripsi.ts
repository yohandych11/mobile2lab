import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { TampilanPage } from '../tampilan/tampilan';

@IonicPage()
@Component({
  selector: 'page-skripsi',
  templateUrl: 'skripsi.html',
})
export class SkripsiPage implements OnInit {
  userForm: FormGroup;
  pembimbing : FormArray;
  checked: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SkripsiPage');
  }

  ngOnInit() {
    this.initializeForm();
  }

  onSubmit() {
      // console.log(this.userForm.value);
      let modal = this.modalCtrl.create(TampilanPage, { data: this.userForm.value});
      modal.present();
      modal.onDidDismiss(
        () => {
          this.userForm.reset();
          this.checked = false;
        }
      )
  }

  private initializeForm() {
    this.userForm = new FormGroup({
      namaDiri: new FormControl(null, Validators.required),
      NIMDiri: new FormControl(null, Validators.required),
      judulSkripsi: new FormControl(null, Validators.required),
      classOf: new FormControl(null, Validators.required),
      pembimbing: new FormArray([new FormControl(null, Validators.required), new FormControl(null)])
    });
  }
}
